﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGenerator : MonoBehaviour {

    static int capacity = 10;
    public GameObject[] cubeContainer = new GameObject[capacity];
    private int currentIndex = 0;
    private Vector3 lastPosition = Vector3.zero;

    public void GenerateCube()
    {
        GameObject cube = null;
        if (cubeContainer[currentIndex] == null)
        {
            cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.parent = transform;
            cubeContainer[currentIndex] = cube;
            cube.name = "cube" + currentIndex;
        }
        else
        {
            cube = cubeContainer[currentIndex];
        }
        lastPosition += new Vector3(cube.transform.localScale.x, 0f, 0f);
        cube.transform.position = lastPosition;
        currentIndex = (currentIndex + 1) % capacity;
    }
}
