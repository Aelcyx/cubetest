﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TimeView : MonoBehaviour {

    public UnityEvent OnEvenTick = new UnityEvent();
    private int lastSecond = -1;
    private bool isEvenEventInvoked = false;
    private bool isTimeEventInvoked = false;

    public Text utcTimeText;

	// Update is called once per frame
	void Update () {
        if (!isEvenEventInvoked || !isTimeEventInvoked)
            CheckTick();
	}

    public void CheckTick()
    {
        int currentSecond = System.DateTime.UtcNow.Second;
        if (lastSecond != currentSecond)
        {
            if (!isTimeEventInvoked)
            {
                InvokeRepeating("SetTimeText", 0f, 1.0f);
                isTimeEventInvoked = true;
            }

            if ((currentSecond % 2 == 0) && !isEvenEventInvoked)
            {
                InvokeRepeating("InvokeEvent", 0f, 2.0f);
                isEvenEventInvoked = true;
            }
        }
        lastSecond = currentSecond;
    }

    public void InvokeEvent()
    {
        OnEvenTick.Invoke();
    }

    public void SetTimeText()
    {
        utcTimeText.text = ToString();
    }

    public override string ToString()
    {
        System.DateTime utcNow = System.DateTime.UtcNow;
        return utcNow.Hour.ToString("00") + ":" + utcNow.Minute.ToString("00") + ":" + utcNow.Second.ToString("00");
    }

}
